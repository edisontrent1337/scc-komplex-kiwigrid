# Controlling of Kiwigrid's Energy Manager over OpenHAB
Kiwigrid's energy manager facilitates connecting and using energy devices like photovoltaic systems, batteries, charging stations and many more. This project aims to evaluate how well the open Home Automation Bus (OpenHAB) functions in this ecosystem.

It is expected to gather data from sensors and devices' behaviour, to store and process these data points and provide them to other applications or optimization purposes.

In its current state this application spawns docker containers used for persistence and displaying of time series based on the gathered information. If desired, it can also create a docker container of openhab.


## Build environment
This project can be compiled and run on every machine supporting [Docker](https://www.docker.com/).  

### Prerequisites
Make sure you have [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/install/) installed.

### Build instructions (Release)
(If you already have an openhab instance up and running)

1. In the root directory of this project execute `docker-compose -f "docker-compose.yml" up -d --build` to launch a composite of Grafana and InfluxDB.
2. Visit `http://<docker-machine>:3000` and follow the [Grafana documentation](http://docs.grafana.org/guides/getting_started/) to configure your data sources and graphs.
3. To shut the service use `docker-compose -f "docker-compose.yml" down`.

### Build instructions (Debug and Test)
(For testing and debugginng purposes. It comes with a docker container of openhab already set up with the necessary bindings, things, etc. for the different smart home plugs.)

1. If you are using an Edimax plug replace `<EDIMAX_IP_ADDRESS>` with its ip address in the file `openhab/conf/things/edimaxPlug.things`.
2. In the root directory of this project execute `docker-compose -f "docker-compose.debug.yml" up -d --build` to launch a composite of Grafana, InfluxDB and openhab.
3. Visit `http://<docker-machine>:3000` and follow the [Grafana documentation](http://docs.grafana.org/guides/getting_started/) to configure your data sources and graphs.
4. To shutdown the service use `docker-compose -f "docker-compose.debug.yml" down`.
