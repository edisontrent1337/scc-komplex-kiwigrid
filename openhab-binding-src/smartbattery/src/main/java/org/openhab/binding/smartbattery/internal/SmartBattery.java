/**
 * Copyright (c) 2010-2019 by the respective copyright holders.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.smartbattery.internal;

import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Energy;

import org.eclipse.smarthome.core.library.types.QuantityType;
import org.eclipse.smarthome.core.library.unit.SmartHomeUnits;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link SmartBattery} represents a battery that can be charged and drained.
 *
 * @author Team KomplexKiwi - Initial contribution
 */
public class SmartBattery {
    private static final QuantityType<Energy> ZERO_CAPACITY = new QuantityType<Energy>(0, SmartHomeUnits.WATT_HOUR);
    private final Logger logger = LoggerFactory.getLogger(SmartBatteryHandler.class);

    private SmartBatteryHandler smartBatteryHandler;
    private QuantityType<Energy> maxCapacity = ZERO_CAPACITY;
    private QuantityType<Energy> remainingCapacity = ZERO_CAPACITY;
    private QuantityType<Dimensionless> stateOfCharge = QuantityType.ZERO;

    public SmartBattery(SmartBatteryHandler smartBatteryHandler) {
        this.smartBatteryHandler = smartBatteryHandler;
    }

    public void chargeAmount(QuantityType<Energy> amount) {
        remainingCapacity = remainingCapacity.add(amount);
        if (remainingCapacity.compareTo(maxCapacity) > 0) {
            remainingCapacity = maxCapacity;
        } else if (remainingCapacity.compareTo(ZERO_CAPACITY) < 0) {
            remainingCapacity = ZERO_CAPACITY;
        }

        if (remainingCapacity.getUnit().equals(SmartHomeUnits.WATT_HOUR) && remainingCapacity.doubleValue() > 10000) {
            remainingCapacity = remainingCapacity.toUnit(SmartHomeUnits.KILOWATT_HOUR);
        } else if (remainingCapacity.getUnit().equals(SmartHomeUnits.KILOWATT_HOUR) && remainingCapacity.doubleValue() < 10) {
            remainingCapacity = remainingCapacity.toUnit(SmartHomeUnits.WATT_HOUR);
        }

        stateOfCharge = (QuantityType<Dimensionless>) remainingCapacity.divide(maxCapacity).toUnit(SmartHomeUnits.PERCENT);
    }

    public void initialize(QuantityType<Energy> initialCapacity) {
        this.stateOfCharge = new QuantityType<Dimensionless>(100, SmartHomeUnits.PERCENT);
        this.maxCapacity = initialCapacity;
        this.remainingCapacity = initialCapacity;
    }

    public QuantityType<Energy> getRemainingCapacity() {
        return remainingCapacity;
    }

    public QuantityType<Dimensionless> getStateOfCharge() {
        return stateOfCharge;
    }
}
