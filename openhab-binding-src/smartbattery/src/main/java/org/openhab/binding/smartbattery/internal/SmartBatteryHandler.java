/**
 * Copyright (c) 2010-2019 by the respective copyright holders.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.smartbattery.internal;

import javax.measure.quantity.Energy;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.smarthome.core.library.types.QuantityType;
import org.eclipse.smarthome.core.library.unit.SmartHomeUnits;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandler;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.State;

/**
 * The {@link SmartBatteryHandler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author Team KomplexKiwi - Initial contribution
 */
@NonNullByDefault
public class SmartBatteryHandler extends BaseThingHandler {
    @Nullable
    private SmartBatteryConfiguration config;

    private SmartBattery smartBattery;

    public SmartBatteryHandler(Thing thing) {
        super(thing);

        this.smartBattery = new SmartBattery(this);
    }

    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        switch (channelUID.getId()) {
            case SmartBatteryBindingConstants.WORK_IN:
                if (command instanceof QuantityType<?>) {
                    smartBattery.chargeAmount((QuantityType<Energy>) command);
                    updateState(SmartBatteryBindingConstants.REMAINING_CAPACITY, smartBattery.getRemainingCapacity());
                    updateState(SmartBatteryBindingConstants.STATE_OF_CHARGE, smartBattery.getStateOfCharge());
                }
                break;
        }
    }

    @Override
    public void initialize() {
        config = getConfigAs(SmartBatteryConfiguration.class);

        smartBattery.initialize(new QuantityType<Energy>(config.maxCapacitykWh, SmartHomeUnits.KILOWATT_HOUR));
        updateState(SmartBatteryBindingConstants.REMAINING_CAPACITY, smartBattery.getRemainingCapacity());
        updateState(SmartBatteryBindingConstants.STATE_OF_CHARGE, smartBattery.getStateOfCharge());
        
        updateStatus(ThingStatus.ONLINE);
    }

    @Override
    public void channelLinked(ChannelUID channelUid) {
        switch (channelUid.getId()) {
            case SmartBatteryBindingConstants.REMAINING_CAPACITY:
                updateState(SmartBatteryBindingConstants.REMAINING_CAPACITY, smartBattery.getRemainingCapacity());
                break;

            case SmartBatteryBindingConstants.STATE_OF_CHARGE:
                updateState(SmartBatteryBindingConstants.STATE_OF_CHARGE, smartBattery.getStateOfCharge());
                break;

            case SmartBatteryBindingConstants.WORK_IN:
                updateState(SmartBatteryBindingConstants.WORK_IN, new QuantityType<Energy>(0, SmartHomeUnits.WATT_HOUR));
                break;
        }
    }
}
