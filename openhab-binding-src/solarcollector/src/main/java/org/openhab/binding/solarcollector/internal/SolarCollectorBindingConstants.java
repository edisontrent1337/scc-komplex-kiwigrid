/**
 * Copyright (c) 2010-2018 by the respective copyright holders.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.solarcollector.internal;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.smarthome.core.thing.ThingTypeUID;

/**
 * The {@link SolarCollectorBindingConstants} class defines common constants, which are
 * used across the whole binding.
 *
 * @author Team - Initial contribution
 */
@NonNullByDefault
public class SolarCollectorBindingConstants {

    private static final String BINDING_ID = "solarcollector";

    // List of all Thing Type UIDs
    public static final ThingTypeUID THING_TYPE_SAMPLE = new ThingTypeUID(BINDING_ID, "collector");

    // List of all Channel ids
    public static final String CURRENT_POWER_OUT = "currentPowerOut";
    public static final String CURRENT_WORK_OUT = "currentWorkOut";
    public static final String DAY_TIME = "dayTime";
    public static final String WORKLOAD = "workload";
    public static final String FORMATTED_TIME = "formattedTime";
}
