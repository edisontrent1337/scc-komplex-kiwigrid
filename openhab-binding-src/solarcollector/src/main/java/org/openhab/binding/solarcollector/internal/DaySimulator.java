/**
 * Copyright (c) 2010-2018 by the respective copyright holders.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.solarcollector.internal;

import java.math.BigDecimal;

import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Energy;
import javax.measure.quantity.Power;
import javax.measure.quantity.Time;

import org.eclipse.smarthome.core.library.types.QuantityType;
import org.eclipse.smarthome.core.library.types.StringType;
import org.eclipse.smarthome.core.library.unit.SmartHomeUnits;

/**
 * The {@link DaySimulator} is responsible for simulating the environment, light intensity time.
 *
 * @author Team - Initial contribution
 */
public class DaySimulator implements Runnable {

    private static final int SIMULATION_SPEED = 24 * 60 / 5;   // 24 * 60 minutes in 5 minutes simulation
    private static final int SIMULATION_START = 6 * 60 * 60;    // 6 o'clock
    private static final QuantityType<Power> MAX_POWER = new QuantityType<Power>(5000, SmartHomeUnits.WATT);

    private static final long DAWN = 7 * 60 * 60;
    private static final long DUSK = 17 * 60 * 60;
    private static final long SUN_DURATION = DUSK - DAWN;
    private static final long TIME_4 = 4 * 60 * 60;
    private static final long TIME_5_30 = (5 * 60 + 30) * 60;
    private static final long TIME_7 = 7 * 60 * 60;
    private static final long TIME_19 = 19 * 60 * 60;

    private long start = 0;
    private long lastUpdate = 0;

    private long daySeconds = 0;
    private double lightIntensity = 0;

    private QuantityType<Power> currentPowerOut = new QuantityType<Power>(0, SmartHomeUnits.WATT);
    private QuantityType<Energy> currentWorkOut = new QuantityType<Energy>(0, SmartHomeUnits.WATT_HOUR);

    private SolarCollectorHandler solarCollectionHandler;

    public DaySimulator(SolarCollectorHandler solarCollectorHandler) {
        this.solarCollectionHandler = solarCollectorHandler;
        start = lastUpdate = System.currentTimeMillis();
        start -= SIMULATION_START * 1000 / SIMULATION_SPEED;
    }

    @Override
    public void run() {
        long now = System.currentTimeMillis();
        double diff = (now - lastUpdate) * SIMULATION_SPEED / 1000.0;
        daySeconds = ((now - start) * SIMULATION_SPEED) % (24 * 60 * 60 * 1000) / 1000;
        lastUpdate = now;

        /*double probability = Math.random();
        double changeMagnitude = 0.02;
        if (probability > 0.9) {
            changeMagnitude = Math.random() * 1.5f;
        } else if (probability > 0.95) {
            changeMagnitude = Math.random() * 0.5f;
        }

        if (daySeconds > TIME_5_30 && daySeconds < TIME_7) {
            if (probability > 0.97) {
                lightIntensity -= 0.5 * changeMagnitude;
            } else {
                lightIntensity += changeMagnitude;
                lightIntensity = Math.min(1, lightIntensity);
            }
        }

        else if (daySeconds >= TIME_19) {
            lightIntensity -= 0.3f * changeMagnitude;
            lightIntensity = Math.max(0, lightIntensity);
        }

        if (daySeconds > TIME_4) {
            lightIntensity += (0.002f + Math.random() * 0.018) * Math.sin(daySeconds / 1728 + Math.random() * 5);
        } else {
            lightIntensity = 0;
        }*/

        if (daySeconds < DAWN || daySeconds > DUSK) {
            lightIntensity = 0.0;
        }
        else {
            lightIntensity = Math.sin(Math.PI / SUN_DURATION * (daySeconds - DAWN));
        }

        //lightIntensity = Math.min(1, Math.max(lightIntensity, 0));
        currentPowerOut = (QuantityType<Power>) MAX_POWER.multiply(BigDecimal.valueOf(lightIntensity));
        currentWorkOut = (QuantityType<Energy>) currentPowerOut.multiply(new QuantityType<Time>(diff, SmartHomeUnits.SECOND)).toUnit(SmartHomeUnits.WATT_HOUR);

        postUpdatesToHandler();
    }

    private void postUpdatesToHandler() {
        solarCollectionHandler.postUpdate(SolarCollectorBindingConstants.WORKLOAD, new QuantityType<Dimensionless>(100 * lightIntensity, SmartHomeUnits.PERCENT));
        solarCollectionHandler.postUpdate(SolarCollectorBindingConstants.CURRENT_POWER_OUT, currentPowerOut);
        solarCollectionHandler.postUpdate(SolarCollectorBindingConstants.CURRENT_WORK_OUT, currentWorkOut);
        solarCollectionHandler.postUpdate(SolarCollectorBindingConstants.DAY_TIME, new QuantityType<Time>(daySeconds, SmartHomeUnits.SECOND));

        long hours = daySeconds / 3600;
        long minutes = (daySeconds / 60) - hours * 60;
        solarCollectionHandler.postUpdate(SolarCollectorBindingConstants.FORMATTED_TIME, new StringType(String.format("%02d:%02d", hours, minutes)));
    }

}
