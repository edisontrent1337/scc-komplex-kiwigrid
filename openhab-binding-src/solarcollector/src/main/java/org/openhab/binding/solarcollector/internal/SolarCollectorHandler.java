/**
 * Copyright (c) 2010-2018 by the respective copyright holders.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.solarcollector.internal;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandler;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.RefreshType;
import org.eclipse.smarthome.core.types.State;

/**
 * The {@link SolarCollectorHandler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author Team - Initial contribution
 */
@NonNullByDefault
public class SolarCollectorHandler extends BaseThingHandler {

    @Nullable
    private SolarCollectorConfiguration config;

    private DaySimulator daySimulator;

    public SolarCollectorHandler(Thing thing) {
        super(thing);
        daySimulator = new DaySimulator(this);
    }

    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        // does nothing
    }

    @Override
    public void initialize() {
        config = getConfigAs(SolarCollectorConfiguration.class);

        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        final ScheduledFuture<?> beepHandler = scheduler.scheduleWithFixedDelay(daySimulator, 2000, 2000, TimeUnit.MILLISECONDS);

        updateStatus(ThingStatus.ONLINE);
    }

    public void postUpdate(String channel, State state) {
        updateState(channel, state);
    }

}
