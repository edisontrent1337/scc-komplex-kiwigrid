/**
 * Copyright (c) 2010-2018 by the respective copyright holders.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.powerinverter.internal;

import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Energy;
import javax.measure.quantity.Time;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.library.types.QuantityType;
import org.eclipse.smarthome.core.library.unit.SmartHomeUnits;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandler;
import org.eclipse.smarthome.core.types.Command;

/**
 * The {@link PowerInverterHandler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author Team KomplexKiwi - Initial contribution
 */
@NonNullByDefault
public class PowerInverterHandler extends BaseThingHandler {

    @Nullable
    private PowerInverterConfiguration config;

    public PowerInverterHandler(Thing thing) {
        super(thing);
    }

    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        // Does nothing
    }

    @Override
    public void initialize() {
        config = getConfigAs(PowerInverterConfiguration.class);
        
        updateState(PowerInverterBindingConstants.EXTERIOR_LIGHTING_SWITCH, OnOffType.OFF);
        updateState(PowerInverterBindingConstants.EXTERIOR_LIGHTING_CURRENT_WORK, new QuantityType<Energy>(0, SmartHomeUnits.WATT_HOUR));
        updateState(PowerInverterBindingConstants.EXTERIOR_LIGHTING_START_TIME, new QuantityType<Time>(config.exteriorLightingStartTime, SmartHomeUnits.SECOND));
        updateState(PowerInverterBindingConstants.EXTERIOR_LIGHTING_END_TIME, new QuantityType<Time>(config.exteriorLightingEndTime, SmartHomeUnits.SECOND));
        updateState(PowerInverterBindingConstants.CAMPING_PLUG_SWITCH, OnOffType.OFF);
        updateState(PowerInverterBindingConstants.CAMPING_PLUG_CURRENT_WORK, new QuantityType<Energy>(0, SmartHomeUnits.WATT_HOUR));
        updateState(PowerInverterBindingConstants.CAMPING_PLUG_WORK_THIS_DAY, new QuantityType<Energy>(0, SmartHomeUnits.WATT_HOUR));
        updateState(PowerInverterBindingConstants.CAMPING_PLUG_MAX_WORK_PER_DAY, new QuantityType<Energy>(config.campingPlugMaxWorkPerDay, SmartHomeUnits.KILOWATT_HOUR));
        updateState(PowerInverterBindingConstants.BATTERY_MIN_STATE_OF_CHARGE, new QuantityType<Dimensionless>(config.batteryMinStateOfCharge, SmartHomeUnits.PERCENT));
        
        updateStatus(ThingStatus.ONLINE);
    }

    @Override
    public void channelLinked(ChannelUID channelUid) {
        switch (channelUid.getId()) {
            case PowerInverterBindingConstants.EXTERIOR_LIGHTING_SWITCH:
                updateState(PowerInverterBindingConstants.EXTERIOR_LIGHTING_SWITCH, OnOffType.OFF);
                break;

            case PowerInverterBindingConstants.EXTERIOR_LIGHTING_CURRENT_WORK:
                updateState(PowerInverterBindingConstants.EXTERIOR_LIGHTING_CURRENT_WORK, new QuantityType<Energy>(0, SmartHomeUnits.WATT_HOUR));
                break;

            case PowerInverterBindingConstants.EXTERIOR_LIGHTING_START_TIME:
                updateState(PowerInverterBindingConstants.EXTERIOR_LIGHTING_START_TIME, new QuantityType<Time>(config.exteriorLightingStartTime, SmartHomeUnits.SECOND));
                break;

            case PowerInverterBindingConstants.EXTERIOR_LIGHTING_END_TIME:
                updateState(PowerInverterBindingConstants.EXTERIOR_LIGHTING_END_TIME, new QuantityType<Time>(config.exteriorLightingEndTime, SmartHomeUnits.SECOND));
                break;

            case PowerInverterBindingConstants.CAMPING_PLUG_SWITCH:
                updateState(PowerInverterBindingConstants.CAMPING_PLUG_SWITCH, OnOffType.OFF);
                break;

            case PowerInverterBindingConstants.CAMPING_PLUG_CURRENT_WORK:
                updateState(PowerInverterBindingConstants.CAMPING_PLUG_CURRENT_WORK, new QuantityType<Energy>(0, SmartHomeUnits.WATT_HOUR));
                break;

            case PowerInverterBindingConstants.CAMPING_PLUG_WORK_THIS_DAY:
                updateState(PowerInverterBindingConstants.CAMPING_PLUG_WORK_THIS_DAY, new QuantityType<Energy>(0, SmartHomeUnits.WATT_HOUR));
                break;

            case PowerInverterBindingConstants.CAMPING_PLUG_MAX_WORK_PER_DAY:
                updateState(PowerInverterBindingConstants.CAMPING_PLUG_MAX_WORK_PER_DAY, new QuantityType<Energy>(config.campingPlugMaxWorkPerDay, SmartHomeUnits.KILOWATT_HOUR));
                break;

            case PowerInverterBindingConstants.BATTERY_MIN_STATE_OF_CHARGE:
                updateState(PowerInverterBindingConstants.BATTERY_MIN_STATE_OF_CHARGE, new QuantityType<Dimensionless>(config.batteryMinStateOfCharge, SmartHomeUnits.PERCENT));
                break;
        }
    }
}
